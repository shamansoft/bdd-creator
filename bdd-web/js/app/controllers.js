var bddApp = angular.module('bddApp', []);

bddApp.controller('DescriptionCtrl', function ($scope) {
    $scope.role="user";
    $scope.action="";
    $scope.reason="I can";
    $scope.scenarios=[];
    $scope.addScenario = function(){
        $scope.scenarios.push({
            "given":"",
            "when":"",
            "then":""
        });
    }
    
    $scope.formatJira1 = function(){
        return "Hello";
    }
    
    $scope.formatJira = function(){
        return "_As&nbsp;a_&nbsp;*"+$scope.role+"*<br/>"+
        "_I&nbsp;want&nbsp;to_&nbsp;"+$scope.action+"<br/>"+
        "_So_&nbsp;{{reason}}<br/>";
                        
    };
    
    $scope.formatJira2 = function(){
        return "_As a_ *"+$scope.role+"*\n"+
        "_I want to_ "+$scope.action+"\n"+
        $scope.reason + "\n";
                        
    };

});